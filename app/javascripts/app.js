import "../stylesheets/app.css";
import abi from './abi.json';

const account = window.web3.eth.accounts[0]; //your metamask address
console.log('your address:'+account);
const contractInstance = web3.eth.contract(abi).at('0xE9159EcE26C78aBb9F8615f4b73C14b42DC5FFaB'); //the address of the contract in testnet ropsten



window.App = {

    mint: function createnewtoken(newuserid) //the id of the new user
    {
        let transactionObject = {
            value: 20000000000000000, //how much money is cost to create new token(0.02 ether)
            gasPrice: 6000000000
        };
        contractInstance.mint.sendTransaction(newuserid, transactionObject, (error, result) => {  console.log(result); }); //call to mint function inside the contract
    },
    totalsupply: function totalsupply() {
        let totalsupply = contractInstance.totalSupply.call(function (error, result) { //how much tokens inside the contract
            if (!error)
                alert(result);
            else
                console.error(error);
        });
    }
};
