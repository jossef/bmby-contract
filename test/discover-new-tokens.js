var web3 = require('web3');
var contract = require('truffle-contract');

var bmby_artifacts =require('../build/contracts/bmby.json');
var bmby = contract(bmby_artifacts);
var q = require('q');


// Setup RPC connection   
var provider = new web3.providers.HttpProvider("http://localhost:8545");

bmby.setProvider(provider);

var lastScannedBmbyId = 0;


function getBmbyToken(bmbyTokenId){

	var defer = q.defer();

	bmby.deployed()
		.then(function(instance) {
      		return instance.getbmby(bmbyTokenId);
    	})
    	.then(defer.resolve)
    	.catch(defer.reject);

	return defer.promise;
}


function iterate(){

	getBmbyToken(lastScannedBmbyId)
		.then((token)=> {
			// add to database 
			console.log(lastScannedBmbyId, token)
			lastScannedBmbyId = lastScannedBmbyId + 1;
			iterate();
		})
		.catch((error)=> console.error('error', lastScannedBmbyId));

}


iterate();
